//
//  BooksApp.swift
//  Books
//
//  Created by Daniel Siancas on 28/01/21.
//

import SwiftUI

@main
struct BooksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
