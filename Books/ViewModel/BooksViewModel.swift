//
//  BooksViewModel.swift
//  Books
//
//  Created by Daniel Siancas on 28/01/21.
//

import SwiftUI
import SwiftyJSON
import Alamofire

class BooksViewModel: ObservableObject {
    @Published var books = [Book]()
    
    init() {
        let url = "https://www.googleapis.com/books/v1/volumes?q=python"
        
        AF.request(url)
            .responseJSON { response in
                if let err = response.error {
                    print(err.localizedDescription)
                    return
                } else if let data = response.data {
                    let json = try! JSON(data: data)
                    
                    let items = json["items"].array ?? []
                    
                    for i in items {
                        let id = i["id"].stringValue
                        let title = i["volumeInfo"]["title"].stringValue
                        let authors = i["volumeInfo"]["authors"].array ?? []
                        var author = ""
                        for j in authors {
                            author += "\(j.stringValue)"
                        }
                        let description = i["volumeInfo"]["description"].stringValue
                        let imurl = i["volumeInfo"]["imageLinks"]["thumbnail"].stringValue
                        let url1 = i["volumeInfo"]["previewLink"].stringValue
                        
                        DispatchQueue.main.async {
                            self.books.append(Book(id: id, title: title, authors: author, desc: description, imgURL: imurl, url: url1))
                        }
                    }
                }
            }
    }
}
