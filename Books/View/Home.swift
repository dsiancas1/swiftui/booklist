//
//  Home.swift
//  Books
//
//  Created by Daniel Siancas on 28/01/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct Home: View {
    @ObservedObject var viewModel = BooksViewModel()
    @State var show = false
    @State var url = ""
    
    var body: some View {
        NavigationView {
            List(viewModel.books) { i in
                NavigationLink(destination: WebView(url: i.url)) {
                    HStack{
                        if i.imgURL != "" {
                            WebImage(url: URL(string: i.imgURL)!).resizable().frame(width: 120, height: 170).cornerRadius(10)
                        }
                        else {
                            Image("books").resizable().frame(width: 120, height: 170).cornerRadius(10)
                        }
                        VStack(alignment: .leading, spacing: 10) {
                            Text(i.title).fontWeight(.bold)
                            Text(i.authors)
                            Text(i.desc).font(.caption).lineLimit(4).multilineTextAlignment(.leading)
                        }
                    }
                }
            }
            .navigationTitle("Books")
            .navigationBarTitleDisplayMode(.inline)
        }
        
    }
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}
