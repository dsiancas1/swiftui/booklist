//
//  WebView.swift
//  Books
//
//  Created by Daniel Siancas on 28/01/21.
//

import SwiftUI
import WebKit

struct WebView: UIViewRepresentable {
    var url : String
      
    func makeUIView(context: Context) -> WKWebView {
        let view = WKWebView()
        view.load(URLRequest(url: URL(string: url)!))
        view.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        return view
    }
      
    func updateUIView(_ uiView: WKWebView, context: Context) {}
}
