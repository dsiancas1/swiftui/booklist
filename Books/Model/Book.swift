//
//  Book.swift
//  Books
//
//  Created by Daniel Siancas on 28/01/21.
//

import Foundation

struct Book: Identifiable {
    var id: String
    var title: String
    var authors: String
    var desc: String
    var imgURL: String
    var url: String
}
